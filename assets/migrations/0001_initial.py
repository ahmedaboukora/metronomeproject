# Generated by Django 2.2.13 on 2020-06-07 14:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Asset',
            fields=[
                ('ISIN', models.TextField(default='', max_length=15, primary_key=True, serialize=False)),
                ('name', models.TextField(default='', max_length=200)),
                ('quantity', models.IntegerField(default=0)),
                ('date', models.DateTimeField(auto_now=True)),
                ('published', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('name', models.CharField(default='', max_length=200)),
                ('id', models.IntegerField(default=0, primary_key=True, serialize=False)),
                ('date', models.DateTimeField(auto_now=True)),
                ('published', models.BooleanField(default=False)),
            ],
        ),
    ]
