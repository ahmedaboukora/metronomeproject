from ib_insync import *
from assets import constants as CST


class IBApi():
    __instance = None
    __connector = IB

    @staticmethod
    def getInstance():
        """ Static access method. """
        if IBApi.__instance == None:
            IBApi()
        return IBApi.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if IBApi.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            IBApi.__instance = self
            IBApi.__connector = IB()

    def connectClient(self): self.__connector.connect(
        CST.HOST_IB, CST.PORT_IB, CST.CURRENT_CLIENT)

    def disconnectClient(self): self.__connector.disconnect()

    def isConnected(self): return self.__connector.isConnected()

    def getLastPrice(self, contract):
        self.__connector.reqMarketDataType(4)
        Ticker = self.__connector.reqMktData(contract)
        while "Last price is not found":
            self.__connector.sleep(5)
            value = Ticker
            if(value.last >= 0.0):
                return value

    def getCurrentPositions(self): return self.__connector.positions()

    def initContract(self, ticker, exchange, currency): return Stock(
        ticker, exchange, currency)

    def initOrder(self, action, quantity): return MarketOrder(action, quantity)

    def placeOrder(self, contract:Contract, order:Order):
        futureData = self.__connector.placeOrder(contract, order)
        while "Order not submitted":
            self.__connector.sleep(5)
            value = futureData
            if(value != None and value.isActive()):
                return value
