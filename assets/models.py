from django.db import models

# Create your models here.
class Asset(models.Model):
    ISIN = models.TextField(max_length=15, blank=False, default='',primary_key=True)
    name = models.TextField(max_length=200,blank=False, default='')
    quantity = models.IntegerField(blank=False, default=0)
    date = models.DateTimeField(auto_now=True)
    published = models.BooleanField(default=False)

class User(models.Model):
    name = models.CharField(max_length=200,blank=False, default='')
    id = models.IntegerField(blank=False, default=0,primary_key=True)
    date = models.DateTimeField(auto_now=True)
    published = models.BooleanField(default=False)

class AssetUser():
    ISIN = str
    name = str
    quantity = int
    date = str
    published = bool
    unitPrice = float
    def __init__(self, name, quantity,unitPrice):
        self.name = name
        self.quantity = quantity
        self.unitPrice = unitPrice