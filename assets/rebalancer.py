from assets.portfolio import Portfolio
import math


class Rebalancer:

    @staticmethod
    def rebalance(portfolio: Portfolio, wanted: dict):
        diffDict = {}
        for asset in wanted:
            currentAsset = portfolio.getAsset(asset)
            current = portfolio.getAssetVolume(currentAsset)
            target = Rebalancer.calculateTarget(
                portfolio, wanted[asset])
            diffDict[asset] = math.ceil(
                (target - current)/currentAsset.unitPrice)
            # print("ASSET = " + asset)
            # print("Current volume  = " + str(current) + " € ")
            # print("Target volume = " + str(target) + " € ")
            # print("How To buy in order to rebalance " +
            #       asset + "  = " + str(diffDict[asset]))
        return diffDict

    @staticmethod
    def calculateTarget(portfolio: Portfolio, pencentage: float) -> float:
        return portfolio.total*(pencentage/100.0)

    @staticmethod
    def applyRebalance(portfolio: Portfolio, rebalanceDict: dict):
        for asset in rebalanceDict:
            indexInList = portfolio.assets.index(portfolio.getAsset(asset))
            portfolio.assets[indexInList].quantity += rebalanceDict[asset]
            portfolio.cash -= rebalanceDict[asset] * portfolio.assets[indexInList].unitPrice
        portfolio.updatePortfolio()