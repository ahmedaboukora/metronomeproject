from django.conf.urls import url
from assets import views
from assets import constants as CST

urlpatterns = [
    url(CST.getAssets, views.asset_list),
    url(CST.getConnect, views.testConnect),
    url(CST.assetDetailURL, views.asset_detail),
    url(CST.assetPublished, views.asset_list_published),
]
