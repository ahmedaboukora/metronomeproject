import json


from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from assets import constants as CST
from assets.portfolio import Portfolio
from assets.models import AssetUser
from assets.rebalancer import Rebalancer
from assets.ibclient.connect import IBApi as ibapi
from ib_insync import Contract
from ib_insync import Order


class RegistrationTestCase (APITestCase):
    clientIBAPI: ibapi
    contract: Contract
    order: Order
    assets: list

    ISHARESWORLD_NAME = "ISHARESWORLD"
    ISHARESWORLD_QTY = 2
    ISHARESWORLD_BUY_PRICE = 38.64

    LYXORCOM_NAME = "LYXOR ETF BX4"
    LYXORCOM_QTY = 33
    LYXORCOM_PRICE = 10
    YOMON_NAME = "YOMONI"
    YOMON_QTY = 1000
    YOMON_PRICE = 1
    ISHARESWORLDETF = AssetUser(
        ISHARESWORLD_NAME, ISHARESWORLD_QTY, ISHARESWORLD_BUY_PRICE)
    LYXORCOMETF = AssetUser(LYXORCOM_NAME, LYXORCOM_QTY, LYXORCOM_PRICE)
    YOMONI_ETF = AssetUser(YOMON_NAME, YOMON_QTY, YOMON_PRICE)
    assetList = [ISHARESWORLDETF, LYXORCOMETF, YOMONI_ETF]

    @classmethod
    def setUpClass(cls):
        cls.clientIBAPI = ibapi()
        cls.clientIBAPI.connectClient()
        cls.contract = cls.clientIBAPI.initContract('CRUD', 'SMART', 'USD')
        cls.order = cls.clientIBAPI.initOrder('BUY', 1)

    @classmethod
    def tearDownClass(cls):
        cls.clientIBAPI.disconnectClient()

    def testServerRun(self):
        response = self.client.get('/'+CST.getConnect)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def testConnectToITWSAPI(self):
        self.assertTrue(self.clientIBAPI.isConnected())

    def testLastPrice(self):
        price = self.clientIBAPI.getLastPrice(self.contract)
        self.assertTrue(price.last == 3.463)

    def testPositions(self):
        self.assertTrue(len(self.clientIBAPI.getCurrentPositions()) == 0)

    # def FX_order(self,symbol):
    #     contract = Contract()
    #     contract.symbol = symbol[:3]
    #     contract.secType = 'CASH'
    #     contract.exchange = 'IDEALPRO'
    #     contract.currency = symbol[3:]
    #     return contract
    # def orderObject(self):
    #     order = Order()
    #     order.action = 'BUY'
    #     order.totalQuantity = 1
    #     order.orderType = 'LMT'
    #     order.lmtPrice = '1.10'
    #     return order

    # def testPlaceOrder(self):
    #     trade = self.clientIBAPI.placeOrder(self.FX_order('EURUSD'), self.orderObject())
    #     self.assertTrue(trade.isActive())

    def testInitPortfolio(self):
        CASH = 600.0
        ISHARESWORLDETF = AssetUser(
            self.ISHARESWORLD_NAME, self.ISHARESWORLD_QTY, self.ISHARESWORLD_BUY_PRICE)
        LYXORCOMETF = AssetUser(
            self.LYXORCOM_NAME, self.LYXORCOM_QTY, self.LYXORCOM_PRICE)
        YOMONI_ETF = AssetUser(
            self.YOMON_NAME, self.YOMON_QTY, self.YOMON_PRICE)
        assetList = [ISHARESWORLDETF, LYXORCOMETF, YOMONI_ETF]
        portfolio = Portfolio(assetList, CASH)
        self.assertTrue(len(assetList) == 3)
        totalAssetValue = self.ISHARESWORLD_QTY*self.ISHARESWORLD_BUY_PRICE + \
            self.LYXORCOM_QTY * self.LYXORCOM_PRICE + self.YOMON_QTY * self.YOMON_PRICE
        totalPortfolio = totalAssetValue + CASH
        self.assertEqual(totalPortfolio, portfolio.total)

    def testcheckAssetRepartitionWithOutCash(self):
        CASH = 600.0
        ISHARESWORLDETF = AssetUser(
            self.ISHARESWORLD_NAME, self.ISHARESWORLD_QTY, self.ISHARESWORLD_BUY_PRICE)
        LYXORCOMETF = AssetUser(
            self.LYXORCOM_NAME, self.LYXORCOM_QTY, self.LYXORCOM_PRICE)
        YOMONI_ETF = AssetUser(
            self.YOMON_NAME, self.YOMON_QTY, self.YOMON_PRICE)
        assetList = [ISHARESWORLDETF, LYXORCOMETF, YOMONI_ETF]
        portfolio = Portfolio(assetList, CASH)
        items = portfolio.calculateRepartion(False)
        #print(items)
        self.assertAlmostEqual(
            items.get(self.ISHARESWORLD_NAME), 5.5, delta=1.0)
        self.assertAlmostEqual(items.get(self.LYXORCOM_NAME), 23.4, delta=1.0)
        self.assertAlmostEqual(items.get(self.YOMON_NAME), 71.1, delta=1.0)
        self.assertAlmostEqual(
            portfolio.reduceOnDict(items), 100.0, delta=1.0)

    def testcheckAssetRepartitionWithCash(self):
        CASH = 600.0
        ISHARESWORLDETF = AssetUser(
            self.ISHARESWORLD_NAME, self.ISHARESWORLD_QTY, self.ISHARESWORLD_BUY_PRICE)
        LYXORCOMETF = AssetUser(
            self.LYXORCOM_NAME, self.LYXORCOM_QTY, self.LYXORCOM_PRICE)
        YOMONI_ETF = AssetUser(
            self.YOMON_NAME, self.YOMON_QTY, self.YOMON_PRICE)
        assetList = [ISHARESWORLDETF, LYXORCOMETF, YOMONI_ETF]
        portfolio = Portfolio(assetList, CASH)
        items = portfolio.calculateRepartion(True)
        self.assertAlmostEqual(
            items.get(self.ISHARESWORLD_NAME), 3.8, delta=1.0)
        self.assertAlmostEqual(
            portfolio.reduceOnDict(items), 100.0, delta=1.0)

    def testRebalance(self):
        CASH = 600.0
        ISHARESWORLDETF = AssetUser(
            self.ISHARESWORLD_NAME, self.ISHARESWORLD_QTY, self.ISHARESWORLD_BUY_PRICE)
        LYXORCOMETF = AssetUser(
            self.LYXORCOM_NAME, self.LYXORCOM_QTY, self.LYXORCOM_PRICE)
        YOMONI_ETF = AssetUser(
            self.YOMON_NAME, self.YOMON_QTY, self.YOMON_PRICE)
        assetList = [ISHARESWORLDETF, LYXORCOMETF, YOMONI_ETF]
        portfolio = Portfolio(assetList, CASH)
        wantedRepartition = {}
        wantedRepartition[self.ISHARESWORLD_NAME] = 20.0
        wantedRepartition[self.LYXORCOM_NAME] = 20.0
        wantedRepartition[self.YOMON_NAME] = 60.0
        items = Rebalancer.rebalance(portfolio, wantedRepartition)
        Rebalancer.applyRebalance(portfolio, items)
        self.assertEquals(len(items), 3)
        yomoniETF = portfolio.getAsset(self.YOMON_NAME)
        iSharesETF = portfolio.getAsset(self.ISHARESWORLD_NAME)
        lyxorETF = portfolio.getAsset(self.LYXORCOM_NAME)
        self.assertEquals(yomoniETF.quantity, 1205)
        self.assertEquals(lyxorETF.quantity, 41)
        self.assertEquals(iSharesETF.quantity, 11)
        self.assertAlmostEquals(
            portfolio.getAssetVolume(yomoniETF), 1205, delta=self.YOMON_PRICE)
        self.assertAlmostEquals(
            portfolio.getAssetVolume(lyxorETF), 401, delta=self.LYXORCOM_PRICE)
        self.assertAlmostEquals(
            portfolio.getAssetVolume(iSharesETF), 401, delta=self.ISHARESWORLD_BUY_PRICE)
        currentRepartition = portfolio.calculateRepartion(False)
        for asset in currentRepartition:
            self.assertAlmostEqual(
                currentRepartition[asset], wantedRepartition[asset],delta=2)
        self.assertTrue(portfolio.cash <= 0.0)
