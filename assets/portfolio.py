from functools import reduce
from assets.models import AssetUser


class Portfolio():
    assets: list
    assetTotalValue: float
    cash: float
    total: float

    def __init__(self, assets, cash):
        self.assets = assets
        self.cash = cash
        self.assetTotalValue = self.calculateTotalAssetValue(
            assets)
        self.total = self.assetTotalValue + self.cash

    def do_sum(self, x1, x2): return x1 + x2

    def calculateTotalAssetValue(self, assets: list): return reduce(
        self.do_sum, list(map(lambda x: self.getAssetVolume(x), assets)))

    def getAssetVolume(
        self, asset: AssetUser): return asset.unitPrice * asset.quantity

    def calculateRepartion(self, withCash: bool):
        items = {}
        for asset in self.assets:
            if(withCash):
                items[asset.name] = self.truncate((self.getAssetVolume(
                    asset)/self.total)*100)
            else:
                items[asset.name] = self.truncate((self.getAssetVolume(
                    asset)/self.assetTotalValue)*100)
            if(withCash):
                items["CASH"] = self.truncate((self.cash/self.total)*100)
        return items

    def reduceOnDict(self, dict) -> int:
        return reduce(self.do_sum, dict.values(), 0)

    def truncate(self, n, decimals=1) -> int:
        multiplier = 10 ** decimals
        return int(n * multiplier) / multiplier

    def getAsset(self, name) -> AssetUser:
        sameName = list(filter(lambda asset: asset.name == name, self.assets))
        return sameName[0]

    def updatePortfolio(self):
        self.assetTotalValue = self.calculateTotalAssetValue(self.assets)
        self.total = self.assetTotalValue + self.cash
