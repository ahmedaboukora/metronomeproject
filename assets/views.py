from django.http.response import JsonResponse
# Create your views here.
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from assets.models import Asset
from assets.serializers import AssetSerializer
from assets.ibclient import connect as ibConnector

@api_view(['GET', 'POST', 'DELETE'])
def asset_list(request):
    if request.method == 'GET':
        assets = Asset.objects.all()

        title = request.GET.get('title', None)
        if title is not None:
            assets = assets.filter(title__icontains=title)

        assets_serializer = AssetSerializer(assets, many=True)
        return JsonResponse(assets_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        asset_data = JSONParser().parse(request)
        asset_serializer = AssetSerializer(data=asset_data)
        if asset_serializer.is_valid():
            asset_serializer.save()
            return JsonResponse(asset_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(asset_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Asset.objects.all().delete()
        return JsonResponse({'message': '{} Assets were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def asset_detail(request, pk):
    try:
        asset = Asset.objects.get(pk=pk)
    except Asset.DoesNotExist:
        return JsonResponse({'message': 'The asset does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        asset_serializer = AssetSerializer(asset)
        return JsonResponse(asset_serializer.data)

    elif request.method == 'PUT':
        asset_data = JSONParser().parse(request)
        asset_serializer = AssetSerializer(asset, data=asset_data)
        if asset_serializer.is_valid():
            asset_serializer.save()
            return JsonResponse(asset_serializer.data)
        return JsonResponse(asset_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        asset.delete()
        return JsonResponse({'message': 'Asset was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def asset_list_published(request):
    assets = Asset.objects.filter(published=True)

    if request.method == 'GET':
        assets_serializer = AssetSerializer(assets, many=True)
        return JsonResponse(assets_serializer.data, safe=False)
@api_view(['GET'])
def testConnect(request):
    return JsonResponse({'message': 'Connected succesfully'}, status=status.HTTP_200_OK)